#!/usr/bin/env bash
set -x

# Install qemu
apt-get update
apt-get -y install qemu-system qemu-efi

# Install kernel build utils
apt-get -y install build-essential libncurses-dev bison flex libssl-dev libelf-dev 

# misc pkgs
apt-get -y install unzip wget bc  debootstrap qemu-user-static

# TODO Setup rootfs
IMG="qemu-image.img"
DIR="target"
qemu-img create $IMG 2g
mkfs.ext4 $IMG
mkdir -p $DIR
for i in {0..7};do
mknod -m 0660 "/dev/loop$i" b 7 "$i"
done

ls -l /dev/loop*
mount -o loop $IMG $DIR
debootstrap --arch=amd64 stretch $DIR http://ftp.de.debian.org/debian/

#Setup autologin 
sed -i 's/9600/9600 --autologin root/g' $DIR/lib/systemd/system/serial-getty@.service

# Setup systemd service
cat << EOF > $DIR/usr/bin/top.sh
#!/bin/bash
echo "**************STARTED**********"
ls -lR /usr/bin
echo "**************DONE**********"
echo "Will shutdown now...."
poweroff
EOF
chmod +x $DIR/usr/bin/top.sh

cat << EOF > $DIR/etc/systemd/system/foo-daemon.service 
[Unit]
Description=execute top.sh on console

[Service]
ExecStart=/usr/bin/top.sh
StandardInput=tty
StandardOutput=tty
TTYPath=/dev/ttyS0

[Install]
WantedBy=getty.target
EOF

## Enable service
ln -s $DIR/etc/systemd/system/foo-daemon.service $DIR/etc/systemd/system/getty.target.wants/foo-daemon.service 

umount $DIR
rmdir $DIR

# Build kernel
wget https://github.com/kdave/btrfs-devel/archive/misc-next.zip
unzip -qq  misc-next.zip

cd btrfs-devel-misc-next/ && make defconfig && make -j8

# Image path : arch/x86/boot/bzImage
# Invoke qemu here
#qemu-system-x86_64 -kernel arch/x86/boot/bzImage -nographic -hda /qemu-image.img  -append "console=ttyS0 root=/dev/sda"

# qemu-system-x86_64 -enable-kvm -m 512 -nographic -kernel arch/x86/boot/bzImage -drive file=/qemu-image.img,index=0,media=disk,format=raw -append "console=ttyS0 root=/dev/sda" 

# qemu-system-x86_64 -enable-kvm -m 512 -nographic -kernel arch/x86/boot/bzImage -drive file=/qemu-image.img,index=0,media=disk,format=raw -append "console=ttyS0 root=/dev/sda" -net user,hostfwd=tcp::2222-:22 -net nic

#qemu-system-x86_64 -enable-kvm -m 512 -nographic -kernel arch/x86/boot/bzImage -drive file=/qemu-image.img,index=0,media=disk,format=raw -append "console=tty1 root=/dev/sda rw"

qemu-system-x86_64 -m 512 -nographic -kernel arch/x86/boot/bzImage -drive file=/qemu-image.img,index=0,media=disk,format=raw -append "console=tty1 root=/dev/sda rw"
